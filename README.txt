No Man's Land
=============
 
Survive in an unfriendly -- some would say unsurvivable -- land between the front lines of a raging
war. Aoid the falling shells, and try to gather supplies without dying in the *No Man's Land*.

## Credits

Based on `minetest_game` with these mods:
 * builtin_item -- TenPlus1
 * playerplus -- TenPlus1
 * yolo -- sofar^[1]
 * snowdrift -- paramat
 * redef -- LinuxDirk
 * snowsong -- texmex
 * raining_death -- aether123^[1]

1: modified
