-- Mud
minetest.register_node("hazards:mud_dry", {
	description = "Dry Mud",
	drawtype = "liquid",
	paramtype = "light",
	tiles = {"hazards_mud_dry.png"},
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = true,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_source = "hazards:mud_wet",
	liquid_alternative_flowing = "default:dirt",
	liquid_viscosity = 7,
	liquid_renewable = false,
	liquid_range = 3,
	post_effect_color = {a=245, r=162, g=107, b=69},
})

minetest.register_node("hazards:mud_wet", {
	description = "Wet Mud",
	drawtype = "liquid",
	paramtype = "light",
	tiles = {"hazards_mud_wet.png"},
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = true,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_source = "hazards:mud_wet",
	liquid_alternative_flowing = "hazards:mud_dry",
	liquid_viscosity = 7,
	liquid_renewable = false,
	liquid_range = 3,
	post_effect_color = {a=245, r=162, g=107, b=69},
})
minetest.register_ore({
	ore_type = "blob",
	ore = "hazards:mud_wet",
	wherein = {"default:dirt_with_grass",
		"default:dirt",
		"default:sand"},
	clust_scarcity = 8 * 8 * 8,
	clust_num_ores = 32,
	clust_size = 6,

	y_min = -512,
	y_max = 128,

	noise_params = {
		offset = 0,
		scale = 1,
		spread = {x = 100, y = 100, z = 100},
		seed = 23,
		octaves = 3,
		persistance = 0.7,
	},
})

--------------- end Mud

-- Barb wire
minetest.register_node("hazards:barb_wire", {
	description = "Barbed Wire",
	drawtype = "plantlike",
	paramtype = "none",
	tiles = {"hazards_barb_wire.png"},
	walkable = false,
	pointable = true,
	diggable = false,
	buildable_to = false,
	is_ground_content = false,
	drop = "",
	damage_per_second = 2,
})

minetest.register_decoration({
	deco_type = "simple",
	place_on = {"default:dirt_with_grass", "default:sand",},
	fill_ratio = 0.1,
	y_min = 0,
	y_max = 128,
	decoration = "hazards:barb_wire",
	height = 1,
	height_max = 2,
})

--------------- end Barb wire

-- Gas
minetest.register_node("hazards:gas_chlorine", {
	description = "Chlorine Gas",
	drawtype = "normal",
	tiles = {"gas.png"},
	walkable = false,
	pointable = false,
	buildable_to = true,
	damage_per_second = 3,
	post_effect_color = {a=45, r=226, g=254, b=188},
})
minetest.register_node("hazards:gas_phosgene", {
	description = "Phosgene Gas",
	drawtype = "normal",
	tiles = {"gas.png"},
	walkable = false,
	pointable = false,
	buildable_to = true,
	damage_per_second = 5,
	post_effect_color = {a=25, r=250, g=250, b=250},
})
minetest.register_node("hazards:gas_mustard", {
	description = "Mustard Gas",
	drawtype = "normal",
	tiles = {"gas.png"},
	walkable = false,
	pointable = false,
	buildable_to = true,
	damage_per_second = 2,
	post_effect_color = {a=45, r=254, g=250, b=188},
})
minetest.register_node("hazards:gas_natural", {
	description = "Natural Gas",
	drawtype = "airlike",
	walkable = false,
	pointable = false,
	buildable_to = true,
	damage_per_second = 4,
})
local gsss = {"chlorine", "phosgene", "mustard"}
for i=1, 3 do
	minetest.register_ore({
		ore_type = "sheet",
		ore = "hazards:gas_" .. gsss[i],
		wherein = "default:air",
		clust_scarcity = 16 * 16 * 16,
		clust_num_ores = 64,
		clust_size = 128,
		y_min = 0,
		y_max = 64,
		noise_params = {
			offset = 0,
			scale = 1.33,
			spread = {x=100, y=100, z=100},
			seed = 23,
			octaves = 3,
			persistance = 0.7,
		},
		column_height_min = 1,
		column_height_max = 12,
		column_midpoint_factor = 0.5,
	})
end
